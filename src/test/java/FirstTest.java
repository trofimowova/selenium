import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstTest {
    WebDriver driver = null;
    String baseUrl = "https://www.google.ru/";
    String expectedTitle = "Google";

    @Test
    public void testGoogleTitle() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);

        String actualTitle = driver.getTitle();
        System.out.println("Title is : " + actualTitle);

       // Assert.assertEquals(actualTitle, expectedTitle);

        driver.quit();
    }
}


