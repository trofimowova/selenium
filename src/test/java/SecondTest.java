import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SecondTest {
    WebDriver driver = null;
    String baseUrl = "https://www.games-workshop.com/en-WW/Home";
    String expectedTitle = "Home | Games Workshop Webstore";

    @Test
    public void testGW() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);

        String actualTitle = driver.getTitle();
        System.out.println("Title is : " + actualTitle);

        Assert.assertEquals(actualTitle, expectedTitle);

        driver.quit();
    }
}