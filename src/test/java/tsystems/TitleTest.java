package tsystems;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TitleTest extends BaseTest{
    String expectedTitle = "My account – Rushplace";

    @Test(description = "Title test")
    public void testRushplaceTitle() {
        Assert.assertEquals(driver.getTitle(), expectedTitle);
    }
}

