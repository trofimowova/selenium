package tsystems;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {
    @Test(description = "Positive login test")
    public void testLogin() throws InterruptedException {

        WebElement EntryFieldBtn = driver.findElement(By.cssSelector(".header_i.header_i_acc :nth-child(2)"));
        EntryFieldBtn.click();


        WebElement usernameInput = driver.findElement(By.name("username"));
        usernameInput.clear();
        usernameInput.sendKeys(Credentials.username);


        // pass field
        WebElement passwordInput = driver.findElement(By.cssSelector("#login_block :nth-child(3) input[type='password']"));
        passwordInput.clear();
        passwordInput.sendKeys(Credentials.password);

        // login button
        WebElement loginButton = driver.findElement(By.cssSelector("#login_block .wr_btn a"));
        loginButton.click();

        //Thread.sleep(15000);

        WebElement CabinetLogo = driver.findElement(By.cssSelector(".header_i_title [href='profile/personal-info.html']"));

        assert CabinetLogo.isDisplayed();
        System.out.println("Authorization is successful! ");

    }




   @Test(description = "Negative login test")
    public void testLoginNegative() throws InterruptedException {

       WebElement EntryFieldBtn = driver.findElement(By.cssSelector(".header_i.header_i_acc :nth-child(2)"));
       EntryFieldBtn.click();


       WebElement usernameInput = driver.findElement(By.name("username"));
       usernameInput.clear();
       usernameInput.sendKeys("Tibald");


       // pass field
       WebElement passwordInput = driver.findElement(By.cssSelector("#login_block :nth-child(3) input[type='password']"));
       passwordInput.clear();
       passwordInput.sendKeys("vvvvv123vvv");

       // login button
       WebElement loginButton = driver.findElement(By.cssSelector("#login_block .wr_btn a"));
       loginButton.click();

       //Thread.sleep(15000);
       assert loginButton.isDisplayed();
       System.out.print("Invalid data for Entry!");


    }
}
