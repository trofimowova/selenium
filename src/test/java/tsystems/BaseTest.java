package tsystems;
import tsystems.capabilities.BrowserCapabilities;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeOptions;


public class BaseTest {
    WebDriver driver = null;
    String url = "https://soap-formula.ru";

    @BeforeMethod
    public void initData() {
        WebDriverManager.chromedriver().setup();
        Capabilities capabilities = BrowserCapabilities.getCapabilities("chrome");
        driver = new ChromeDriver((ChromeOptions) capabilities);


        //driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(url);
    }

    @AfterMethod
    public void stop() {
        driver.quit();
    }
}

